package cat.escolapia.pmdm.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import cat.escolapia.pmdm.DemoScene2DHit;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new DemoScene2DHit(), config);
		config.title = "Pilotes";
		config.width  = 1024;
		config.height = 600;
	}
}
